const mongoose = require("mongoose");



const userSchema = new mongoose.Schema({
	
	name: {
		type: String,
		required: [true, "Name is required"]
	},

	email: {
		type: String,
		required: [true, "Email is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"]
	},
	password: {
		type: String,
		required: [true, "Passwword is required"]
	},
	isAdmin :{
		type: Boolean,
		default: false
	
	}
})

module.exports = mongoose.model("User", userSchema);